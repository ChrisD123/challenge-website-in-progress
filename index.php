<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Challenge Gaming</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
<div id="header">
    <h1> Challenge Website</h1>
    <div id="links">
        <a href="login.php">Login</a>
        <a href="registration.php">Register</a>
    </div>
</div>

<p class = "main"> Keep an eye out for challenges and pit yourself against other players to win!</p>
<p class = "main"> You can also issue friendly challenges to your friends.</p>
<div id="footer">
<p> Chris Dunham <br />
chrisdunham0@gmail.com</p>
</div>
</body>
</html>