<php
REQUIRE_ONCE('myfunctions.php'); // Include functions php file

// Store database connection variable
$db = getConnection();

// Start session
session_start();

// If username session variable is set, redirect user to index page
if(isset($_SESSION['username']))
{
header('Location: index.php');
}
 
// If submit button has been pressed, the following code is executed
if(isset($_POST['register']))
{
    // Checks if all fields are not empty
    if(isset($_POST['email']) && isset($_POST['userpassword'] && isset($_POST['confirmpassword'] && isset($_POST['username']))
    {
    
    // Stores user input into variables
    $email = trim($_POST['email']);
    $password = trim($_POST['userpassword']);
    $confirmpassword = trim($_POST['confirmpassword']);
    $username = trim($_POST['username']);
     
        // Checks if the entered e-mail is a valid e-mail address
        if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
        {
            alertMessage('Please enter a valid e-mail address.');
        }
        // Checks if entered password matches the requirements
        elseif (!preg_match("#[0-9]+#", $password) || !preg_match("#[A-Z]+#", $password) || strlen($password) < 6 || strlen($password) > 12)
        {
            alertMessage('Passwords must have at least 1 capital letter, 1 number and be between 6 and 12 characters long.');
        }
        // Checks if password and confirm password fields match
        elseif (strcmp($password, $confirmpassword) != 0) 
        {
            alertMessage('Password and Confirm Password fields do not match.');
        }
        // Checks if entered username matches requirements
        elseif (!preg_match("#[0-9]+#", $username) || !preg_match("#[A-Z]+#", $username) || strlen($username) < 6 || strlen($username) > 18) 
        {
            alertMessage('Username must contain 1 uppercase letter, a number and between 6 and 18 characters long.');
        }
        else
        {
            // Hashes the entered password and stores it in a new variable
            $passwordhash = hash('md5', $password);
            
            // Inserts new record into user table with the balues the user has entered
            $createstatement = "INSERT INTO user (email, password, username) VALUES('$email', '$password', '$username')";
            $createuser = $db->query($createstatement);
             
            // Stores the username session variable
            $_SESSION['username'] = $username;
             
            // Variables used to send an e-mail to the user who has just registered
            $to = $email;
            $subject = "Registration";
            $text = "Welcome to the website! Please log in with your details.";
            $headers = "From: admin@gamechallenge.com";
             
            // Sends the new user an e-mail
            mail($to, $subject, $text, $headers);
        }
     
    }
    else
    {
        // Displays message if any fields are empty
        alertMessage('Please fill in all fields.');
    }
}
 
 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Challenge Gaming</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
<div id="header">
    <h1> Challenge Website</h1>
    <div id="links">
        <a href="login.php">Login</a>
        <a href="registration.php">Register</a>
    </div>
</div>
<h2> Registration </h2>
<div id="registration">
<form method="post" action="registration.php">
    E-mail Address: <br />
    <div class="formspace" data-tip="Username can contain up to 20 alphanumeric characters.">
        <input type="text" name="email" required/> <br />
    </div>
    Password: <br />
    <div class="formspace" data-tip="Password must be between 6 and 10 characters long, and only contain letters and numbers.">
        <input type="password" name="userpassword" required/> <br />
    </div>
    Confirm Password: <br />
    <div class="formspace" data-tip="Re-enter your password.">
        <input type="password" name="confirmpassword" required/>
    </div>
    Username: <br />
    <div class="formspace" data-tip="Username can be 10-20 characters long, and ,ust only contain letters and numbers.">
        <input type="text" name="username" required/>
    </div>
    <input type="submit" name="register" value="Register" />
</form>
</div>
</body>
</html>