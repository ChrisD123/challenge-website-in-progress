<?php
/**
* Displays a message with the text specified by the parameter variable
*/
function alertMessage($message)
{
echo "<script type='text/javascript'>alert('$message')</script>";
}
 
/**
* Establishes a connection and returns a database object variable
*/
function getConnection()
{
try {
    $db = new PDO('mysql:host=localhost;dbname=database', 'user', 'root');
    return $db;
    }
    catch(PDOException$e)
    {
    echo "Connection Error: " . $e->getMessage();
    }
}
?>